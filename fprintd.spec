Name:             fprintd
Version:          0.8.1
Release:          4
Summary:          Support for consumer fingerprint reader devices
License:          GPLv2+
Url:              http://www.freedesktop.org/wiki/Software/fprint/fprintd
Source0:          https://gitlab.freedesktop.org/libfprint/fprintd/uploads/bdd9f91909f535368b7c21f72311704a/%{name}-%{version}.tar.xz

BuildRequires:    dbus-glib-devel pam-devel libfprint-devel >= 0.1.0 polkit-devel gtk-doc
BuildRequires:    intltool autoconf automake libtool perl-podlators pkgconfig(systemd)

Provides:         pam_fprint = %{version}-%{release} %{name}-pam = %{version}-%{release}
Obsoletes:        pam_fprint < 0.2-3 %{name}-pam < %{version}-%{release}

Requires(postun): authselect >= 0.3

%description
The fprint project aims to add support for consumer fingerprint reader devices, in Linux, as well as other free Unices.

%package devel
Summary:          Development package for %{name}
Requires:         %{name} = %{version}-%{release}
License:          GFDLv1.1+
BuildArch:        noarch

%description devel
This package contains some libraries and header files for the
development of %{name}.

%package help
Summary:          Help package for %{name}
Requires:         %{name} = %{version}-%{release}
BuildArch:        noarch

%description help
This package contains some man help files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --libdir=%{_libdir} --enable-gtk-doc --enable-pam

%make_build

%install
rm -rf %{buildroot}
%make_install
install -d %{buildroot}%{_localstatedir}/lib/fprint

%find_lang %{name}

%postun
if [ $1 -eq 0 ]
then
    /bin/authselect disable-feature with-fingerprint || :
fi

%files -f %{name}.lang
%doc README pam/README COPYING AUTHORS TODO
%{_libdir}/security/pam_fprintd.so
%{_bindir}/fprintd-*
%{_libexecdir}/fprintd
%{_sysconfdir}/fprintd.conf
%{_sysconfdir}/dbus-1/system.d/net.reactivated.Fprint.conf
%{_datadir}/dbus-1/system-services/net.reactivated.Fprint.service
%{_datadir}/polkit-1/actions/net.reactivated.fprint.device.policy
/usr/lib/systemd/system/fprintd.service
%{_localstatedir}/lib/fprint
%exclude %{_libdir}/security/pam_fprintd.{a,la,so.*}

%files devel
%{_datadir}/gtk-doc/
%{_datadir}/dbus-1/interfaces/net.reactivated.Fprint.{Device,Manager}.xml

%files help
%{_mandir}/man1/fprintd.1.gz

%changelog
* Mon Nov 04 2019 huzhiyu <huzhiyu1@huawei.com> - 0.8.1-4
- Package init
